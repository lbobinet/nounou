extern crate nounou;

use serde::ser;
use clap::{App, Arg, SubCommand};
use std::fs;
use std::io::prelude::*;

use nounou::{Error, Contract, ContractEditableProperty, ask_confirmation};

fn main() -> Result<(), Error> {
    let matches = App::new("Nounou")
        .arg(Arg::with_name("contrat").short("c").required(false).takes_value(true).help("Nom du contrat"))
        .arg(Arg::with_name("file").short("f").required(false).takes_value(true).help("fichier"))
        .subcommand(SubCommand::with_name("init"))
        .subcommand(SubCommand::with_name("infos"))
        .subcommand(SubCommand::with_name("test"))
        .subcommand(SubCommand::with_name("paye"))
        .subcommand(SubCommand::with_name("congé"))
        .subcommand(SubCommand::with_name("edit")
            .subcommand(SubCommand::with_name("salaire"))
            .subcommand(SubCommand::with_name("frais")))
        .get_matches();

    let env_filepath = std::env::var("DEFAULT_NOUNOU_FILE").ok();
    let filepath = matches.value_of("file").map(|r|String::from(r))
        .or(env_filepath).ok_or(Error {text: "No file specified"})?;

    match matches.subcommand() {
        ("init", Some(_)) => create_new_contract_file(&filepath)?,
        ("infos", Some(_)) => open_and_op(&filepath, Contract::show)?,
        ("paye", Some(_)) => open_and_op_mut(&filepath, Contract::add)?,
        ("congé", Some(_)) => open_and_op_mut(&filepath, Contract::add_leave)?,
        ("test", Some(_)) => open_and_op(&filepath, Contract::test)?,
        ("edit", Some(sub_m)) =>
            match sub_m.subcommand() {
                ("salaire", Some(_)) => open_and_op_mut_with_arg(
                    &filepath, Contract::edit_property, ContractEditableProperty::HourlyWage)?,
                ("frais", Some(_)) => open_and_op_mut_with_arg(
                    &filepath, Contract::edit_property, ContractEditableProperty::DailyParticipation)?,
                _ => {
                    println!("no sub command recognized");
                },
            }
        _ => {},
    }
    Ok(())
}

fn create_new_contract_file(filepath: &str) -> Result<(), Error> {
    let test_already_exists = fs::File::open(&filepath);
    if let Ok(_) = test_already_exists {
        println!("file {} already exists", filepath);
    } else {
        let contract = Contract::init()?;
        save_application_file(&contract, filepath)?;
        println!("create new contract in {}", filepath);
    }
    Ok(())
}

fn save_application_file<T: ser::Serialize>(contract: &T, filepath: &str) -> Result<(), Error> {
    match toml::to_string(&contract) {
        Ok(serialized) => fs::write(filepath, serialized).map_err(|_| Error{text: "fs write error"}),
        Err(_) => Err(Error{text: "get path error"})
    }
}

fn open_and_op<F: Fn(&Contract) -> Result<(), Error>>(filepath: &str, op: F) -> Result<(), Error> {
    let contract = open_contract_data(&filepath)?;
    op(&contract)
}

fn open_and_op_mut<F: Fn(&mut Contract) -> Result<(), Error>>(filepath: &str, op: F) -> Result<(), Error> {
    let mut contract = open_contract_data(filepath)?;
    op(&mut contract)?;
    if ask_confirmation()? {
        save_application_file(&contract, filepath)
    } else {
        Ok(())
    }
}

fn open_and_op_mut_with_arg<A, F: Fn(&mut Contract, A) -> Result<(), Error>>(filepath: &str, op: F, arg: A) -> Result<(), Error> {
    let mut contract = open_contract_data(filepath)?;
    op(&mut contract, arg)?;
    if ask_confirmation()? {
        save_application_file(&contract, filepath)
    } else {
        Ok(())
    }
}

fn open_contract_data(filepath: &str) -> Result<Contract, Error> {
    let mut file = fs::File::open(filepath).map_err(|_| Error{text: "fs open error"})?;
    let mut content = String::new();
    file.read_to_string(&mut content).map_err(|_| Error{text: "fs read error"})?;
    match toml::from_str(&content) {
        Ok(contract) => Ok(contract),
        Err(_) => Err(Error{text: "toml deserialize error"})
    }
}
