use serde_derive::{Serialize, Deserialize};
use chrono::prelude::*;
use time::Duration;
use std::fmt;
use std::io;
use std::io::Write;
use std::ops::Range;
use::std::str::FromStr;
use std::collections::HashMap;
use std::cmp::{max};

pub const DATE_FMT: &str = "%d/%m/%Y";

#[derive(Copy, Clone, Debug, Default, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Error {
    pub text: &'static str
}

impl From<io::Error> for Error {
    fn from(_: io::Error) -> Self {
        Error{text: "IO error"}
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Contract {
    start_date: NaiveDate,
    current_hourly_wage: f32,
    daily_participation: f32,
    nb_weeks_per_year: u32,
    payments: Vec<Payment>,
    leave_periods: Vec<Range<NaiveDate>>,
    planning: HashMap<Weekday, f32>
}

impl Default for Contract {
    fn default() -> Self {
        Contract {
            start_date: Local::now().naive_local().date(),
            current_hourly_wage: 0f32,
            daily_participation: 0f32,
            nb_weeks_per_year: 0u32,
            payments: Vec::new(),
            leave_periods: Vec::new(),
            planning: HashMap::new()
        }
    }
}

#[derive(Debug)]
pub enum ContractEditableProperty {
    HourlyWage,
    DailyParticipation
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Payment {
    start_date: NaiveDate,
    end_date: NaiveDate,
    payment_date: NaiveDate,
    hourly_wage: f32,
    nb_hours_per_week: f32,
    nb_worked_days: u32,
    nb_compl_hours: f32,
    nb_maj_hours: f32,
    net_amount: f32,
    amount: f32
}

impl fmt::Display for Payment {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} -> {}: {} payed on {}", 
            self.start_date.format(DATE_FMT), 
            self.end_date.format(DATE_FMT),
            self.amount,
            self.payment_date.format(DATE_FMT))
    }
}

impl Contract {
    fn new(start_date: NaiveDate,
        current_hourly_wage: f32,
        daily_participation: f32,
        nb_weeks_per_year: u32,
        planning: HashMap<Weekday, f32>) -> Contract {
        Contract {
            start_date,
            current_hourly_wage,
            daily_participation,
            nb_weeks_per_year,
            planning,
            ..Default::default()
        }
    }

    pub fn test(&self) -> Result<(), Error> {
        Ok(())
    }

    pub fn edit_property(&mut self, prop: ContractEditableProperty) -> Result<(), Error> {
        match prop {
            ContractEditableProperty::HourlyWage => 
                self.current_hourly_wage = ask_value("Salaire horaire", Some(self.current_hourly_wage))?,
            ContractEditableProperty::DailyParticipation => 
                self.daily_participation = ask_value("Frais d'entretien", Some(self.daily_participation))?
        };
        Ok(())
    }

    pub fn init() -> Result<Contract, Error> {
        let start_date = ask_date("Date de début", None)?;
        let nb_weeks = ask_value("Nombre de semaines par an", None)?;
        let hourly_wage = ask_value("Salaire horaire", None)?;
        let daily_participation = ask_value("Frais d'entretien quotidiens", None)?;
        let planning = ask_planning()?;
        Ok(Contract::new(start_date, hourly_wage, daily_participation, nb_weeks, planning))
    }

    pub fn show(&self) -> Result<(), Error> {
        println!("--------------------------------------------");
        println!("Date de début:\t\t\t{}", self.start_date.format(DATE_FMT));
        println!("Salaire horaire:\t\t{}", self.current_hourly_wage);
        println!("Frais d'entretien quotidiens:\t{}", self.daily_participation);
        println!("Nombre d'heures par semaine:\t{}", self.nb_hours_per_week());
        println!("Salaire mensualisé:\t\t{}", self.monthly_wage());
        println!("--------------------------------------------");

        let current_date = self.first_unpayed_day();
        if let Some(ref_year) = self.ref_year_for(&current_date) {
            println!("Année de référence");
            println!("--------------------------------------------");
            self.leave_daily_wage(&ref_year, true);
            println!("Total net:\t\t\t{} €", self.total_net(&ref_year));
            println!("--------------------------------------------");
        }
        
        let next_year_date = NaiveDate::from_ymd(current_date.year() + 1, current_date.month(), current_date.day());
        if let Some(ref_year) = self.ref_year_for(&next_year_date) {
            let period = Range{start: ref_year.start, end: current_date};
            println!("Année en cours");
            println!("--------------------------------------------");
            self.leave_daily_wage(&period, true);
            println!("Total net:\t\t\t{} €", self.total_net(&period));
            println!("--------------------------------------------");
        }
        Ok(())
    }

    pub fn add(&mut self) -> Result<(), Error> {
        let start_period = ask_date("Du", Some(self.first_unpayed_day()))?;
        let end_period = ask_date("Au", Some(last_day_of_same_month(&start_period)))?;
        let nb_days = ask_value("Nombre de jours travaillés", Some(self.compute_nb_workday(&start_period, &end_period)))?;
        let participation = nb_days as f32 * self.daily_participation;
        let wage = round_amount(self.monthly_wage());

        let max_compl_hours = 45.0 - self.nb_hours_per_week();
        let suppl_hours: f32 = ask_value("Heures supplémentaires", Some(0f32))?;

        let compl_hours = if suppl_hours > max_compl_hours { max_compl_hours } else { suppl_hours };
        let majored_hours = suppl_hours - compl_hours;

        let leave_remuneration = round_amount(self.leave_remuneration(&start_period) / 12.0);
        if leave_remuneration > 0f32 {
            println!("Indemnités congés payés par mois: {}", leave_remuneration);
        }

        let compl_hours_amount = round_amount(compl_hours * self.current_hourly_wage);
        let majored_hours_amount = round_amount(majored_hours * self.current_hourly_wage * 1.15);
        let computed_net_amount = round_amount(wage + leave_remuneration + compl_hours_amount + majored_hours_amount);
        
        println!("--------------------------------------------");
        println!("Salaire mensuel anualisé:\t\t{}", wage);
        println!("Heures complémentaires ({}):\t\t{}", compl_hours, compl_hours_amount);
        println!("Heures majorées ({}):\t\t\t{}", majored_hours, majored_hours_amount);
        println!("--------------------------------------------");
        println!("Total net:\t\t\t\t{}", computed_net_amount);
        println!("--------------------------------------------");
        println!("Frais d'entretien:\t\t\t{}", participation);
        println!("--------------------------------------------");
        println!("Total:\t\t\t\t\t{}", computed_net_amount + participation);
        println!("--------------------------------------------");

        let net_amount: f32 = ask_value("Salaire net", Some(computed_net_amount))?;
        let amount: f32 = ask_value("Montant versé", Some(round_amount(net_amount + participation)))?;
        let payment_date = ask_date("Date de paiement", Some(end_period))?;


        let new_payment = Payment {
            start_date: start_period,
            end_date: end_period,
            payment_date,
            nb_worked_days: nb_days,
            nb_maj_hours: majored_hours,
            nb_compl_hours: compl_hours,
            net_amount,
            amount,
            hourly_wage: self.current_hourly_wage,
            nb_hours_per_week: self.nb_hours_per_week()
        };
        self.payments.push(new_payment);
        return Ok(())
    }

    pub fn add_leave(&mut self) -> Result<(), Error> {
        let start_period = ask_date("Du", None)?;
        let end_period = ask_date("Au", None)?;
        let period = Range{start: start_period, end: end_period};
        self.leave_periods.push(period);
        Ok(())
    }

    fn first_unpayed_day(&self) -> NaiveDate {
        let mut last_payed_date: Option<NaiveDate> = None;
        for payment in &self.payments {
            match last_payed_date {
                None => last_payed_date = Some(payment.end_date + Duration::days(1)),
                Some(val) => {
                    if val < payment.end_date {
                        last_payed_date = Some(payment.end_date + Duration::days(1))
                    }
                }
            }
        }
        last_payed_date.unwrap_or(self.start_date)
    }

    fn compute_nb_workday(&self, start:&NaiveDate, end:&NaiveDate) -> u32 {
        let mut current_day = *start;
        let mut worked_days = 0;
        while current_day <= *end {
            if self.is_worked(&current_day) {
                match self.planning.get(&current_day.weekday()) {
                    Some(hours) => if *hours > 0f32 {
                        worked_days += 1;
                    },
                    None => {}
                }
            }
            current_day = current_day.succ();
        }
        worked_days
    }

    fn is_worked(&self, date: &NaiveDate) -> bool {
        self.leave_periods.iter().find(|r| r.start <= *date && r.end >= *date).is_none()
    }

    fn nb_hours_per_week(&self) -> f32 {
        self.planning.values().fold(0f32, |acc, v| acc + v)
    }

    fn monthly_wage(&self) -> f32 {
        round_amount(self.current_hourly_wage * self.nb_hours_per_week() * self.nb_weeks_per_year as f32 / 12f32)
    }

    fn ref_year_for(&self, base_date: &NaiveDate) -> Option<Range<NaiveDate>> {
        let delta_year = if base_date.month() > 5 {0} else {1};
        let current_year_start = NaiveDate::from_ymd(base_date.year() - delta_year, 6, 1);
        if current_year_start <= self.start_date {
            return None;
        }
        let last_year_theoretical_start = NaiveDate::from_ymd(current_year_start.year() - 1, 6, 1);
        let last_year_start = max(last_year_theoretical_start, self.start_date);
        Some(Range{start: last_year_start, end: current_year_start})
    }

    fn payments_for_period(&self, period: &Range<NaiveDate>) -> Vec<&Payment> {
        self.payments.iter().filter(
            |p| period.start <= p.start_date && period.end > p.start_date)
            .collect::<Vec<&Payment>>()
    }

    fn leave_10_percent(&self, period: &Range<NaiveDate>) -> f32 {
        round_amount(self.total_net(&period) * 0.1)
    }

    fn total_net(&self, period: &Range<NaiveDate>) -> f32 {
        let period_payments = self.payments_for_period(&period);
        let total_net_amount = period_payments.iter().fold(0f32, |acc, p| acc + p.net_amount);
        round_amount(total_net_amount)
    }

    fn leave_daily_wage(&self, period: &Range<NaiveDate>, display: bool) -> f32 {
        let payments = self.payments_for_period(&period);
        let nb_eligible_days = self.compute_nb_workday(&period.start, &period.end);
        //let nb_eligible_days = payments.iter().fold(0u32, |acc, p| acc + p.nb_worked_days);
        let nb_days_per_week = self.planning.iter().filter(|(_, v)| **v > 0f32).count();
        let nb_weeks = nb_eligible_days / nb_days_per_week as u32;
        let nb_aquired_days = nb_weeks as f32 / 4.0 * 2.5;
        let average_daily_wage = payments.iter().fold(0f32, |acc, p| 
            acc + p.nb_hours_per_week * p.hourly_wage) / 6.0 / payments.len() as f32;
        let year_value = nb_aquired_days.ceil() * average_daily_wage;

        if display {
            println!("Jours travaillés:\t\t{}", nb_eligible_days);
            println!("Equivalent semaines:\t\t{}", nb_weeks);
            println!("Jours ouvrables aquis:\t\t{}", nb_aquired_days.ceil());
            println!("Salaire journalier moyen:\t{}", round_amount(average_daily_wage));
            println!("Montant aquis:\t\t\t{}", round_amount(year_value));
        }

        year_value
    }

    fn leave_remuneration(&self, base_date: &NaiveDate) -> f32 {
        match self.ref_year_for(base_date) {
            Some(ref_year) => {
                let result1 = self.leave_10_percent(&ref_year);
                let result2 = self.leave_daily_wage(&ref_year, false);
                if result1 > result2 {result1} else {result2}
            },
            None => 0f32
        }
    }
}

pub fn ask_planning() -> Result<HashMap<Weekday, f32>, Error> {
    let mut planning: HashMap<Weekday, f32> = HashMap::new();
    println!("Nombres d'heures:");
    planning.insert(Weekday::Mon, ask_value("Lundi", None)?);
    planning.insert(Weekday::Tue, ask_value("Mardi", None)?);
    planning.insert(Weekday::Wed, ask_value("Mercredi", None)?);
    planning.insert(Weekday::Thu, ask_value("Jeudi", None)?);
    planning.insert(Weekday::Fri, ask_value("Vendredi", None)?);
    planning.insert(Weekday::Sat, ask_value("Samedi", None)?);
    planning.insert(Weekday::Sun, ask_value("Dimanche", None)?);
    Ok(planning)
}

fn round_amount(value: f32) -> f32 {
    (value * 100.0).round() / 100.0
}

fn last_day_of_same_month(base_date: &NaiveDate) -> NaiveDate {
    
    let first_of_next_month = if base_date.month() == 12 {
        NaiveDate::from_ymd(base_date.year() + 1, 1, 1)
    } else {
        NaiveDate::from_ymd(base_date.year(), base_date.month() + 1, 1)
    };
    first_of_next_month + Duration::days(-1)
}

fn ask_data<E, T: fmt::Display + Copy + Clone, F: Fn(&str) -> Result<T, E>>
    (msg: &str, convert: F, default_str: Option<String>) -> Result<T, Error> {

    loop {
        let input = ask_line_input(&msg, &default_str);
        match input {
            Err(error) => {
                return Err(error)
            },
            Ok(text) => {
                let converted = convert(&text);
                match converted {
                    Ok(val) => return Ok(val),
                    Err(_) => ()
                }
            }
        };
    }
}

pub fn ask_date(msg: &str, default_value: Option<NaiveDate>) -> Result<NaiveDate, Error> {
    let default_text = default_value.map(|val| val.format(DATE_FMT).to_string());
    ask_data(msg, |text| NaiveDate::parse_from_str(&text, DATE_FMT), default_text)
}

fn ask_value<T: FromStr + ToString + Copy + fmt::Display>(msg: &str, default: Option<T>) -> Result<T, Error> {
    ask_data(msg, |text| T::from_str(text), default.map(|v| v.to_string()))
}

pub fn ask_confirmation() ->  Result<bool, Error> {
    ask_data("confirmer ?", |v| match v {
        "y" => Ok(true),
        "Y" => Ok(true),
        "n" => Ok(false),
        "N" => Ok(false),
        "y/N" => Ok(false),
        _ => Err("no bool")
    }, Some(String::from("y/N")))
}

fn ask_line_input<'a>(msg: &str, default_str: &Option<String>) -> Result<String, Error> {
    let stdin = std::io::stdin();
    let input = &mut String::new();

    print!("{}", match default_str {
        Some(def_value) => format!("{} [{}]: ", msg, def_value),
        None => format!("{}: ", msg)
    });
    io::stdout().flush().map_err(|_| Error{text: "flush error"})?;
    input.clear();
    stdin.read_line(input).map_err(|_| Error {text: "readline error"})?;
    let text = String::from(input.trim());
    if text.len() == 0 {
        default_str.clone().ok_or(Error {text: "vide"})
    } else {
        Ok(text)
    }
}
